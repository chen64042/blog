class AddPostRefToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :post_id, index: true, foreign_key: true
  end
end
